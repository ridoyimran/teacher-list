<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>
<html>
	<head>
		<title>Teacher</title>
		<link type="text/css" rel="stylesheet"
			href="${pageContext.request.contextPath}/resources/css/style.css"/>
			
		<link type="text/css" rel="stylesheet"
			href="${pageContext.request.contextPath}/resources/css/add-customer-style.css"/>
	</head>
	<body>
		<div id="wrapper">
			<div id="header">
				<h2>Teacher Management System</h2>
			</div>
		</div>
		
		<div id="container">
			<div id="content">
				<h3>Teacher Form</h3>
				
				<form:form action="process" modelAttribute="teacherForm" method="POST">
				<form:hidden path="id" />
			<table>
				<tbody>
					<tr>
						<td><label>First Name: </label></td>
						<td> <form:input path="fname"/> </td>
					</tr>
					<tr>
						<td><label>Last Name: </label></td>
						<td> <form:input path="lname"/> </td>
					</tr>	
					<tr>
						<td><label>Email: </label></td>
						<td> <form:input path="email"/> </td>
					</tr>
					<tr>	
						<td><label></label></td>
						<td> <input type="submit" value="save" class="save"/> </td>
					</tr>
				</tbody>
			</table>
			
		</form:form>
				
				
			</div>
		</div>
	</body>
</html>