package com.lesson.springdemo.service;

import java.util.List;

import com.lesson.springdemo.entity.Teacher;

public interface TeacherService {

	public List<Teacher> getTeachers();

	public void getTeacher(Teacher theTeacher);

	public Teacher getUpdateTeacher(int theId);

	public void deleteTeacherhistory(int theId);
}
