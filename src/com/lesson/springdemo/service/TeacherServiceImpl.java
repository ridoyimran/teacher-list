package com.lesson.springdemo.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lesson.springdemo.dao.TeacherDAO;
import com.lesson.springdemo.entity.Teacher;

@Service
public class TeacherServiceImpl implements TeacherService {

	@Autowired
	private TeacherDAO teacherDAO;
	@Override
	@Transactional
	public List<Teacher> getTeachers() {
		
		return teacherDAO.getTeachers();
	}
	@Override
	@Transactional
	public void getTeacher(Teacher theTeacher) {
		
		teacherDAO.getTeacher(theTeacher);
		
	}
	@Override
	@Transactional
	public Teacher getUpdateTeacher(int theId) {
		return teacherDAO.getUpdateTeacher(theId);
	}
	@Override
	@Transactional
	public void deleteTeacherhistory(int theId) {
		teacherDAO.deleteTeacherhistory(theId);
		
	}

}
