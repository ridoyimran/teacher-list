package com.lesson.springdemo.aspect;

import java.util.logging.Logger;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class MyLoggingAspect {
	
	private Logger myLogger = Logger.getLogger(getClass().getName());

	@Pointcut("execution(* com.lesson.springdemo.controller.*.*(..))")
	private void forController() {}
	
	@Pointcut("execution(* com.lesson.springdemo.service.*.*(..))")
	private void forService() {}
	
	@Pointcut("execution(* com.lesson.springdemo.dao.*.*(..))")
	private void forDao() {}
	
	@Pointcut("forController() || forService() || forDao()")
	private void forApply() {}
	
	@Before("forApply()")
	public void usingBefore(JoinPoint theJoinPoint) {
		String value = theJoinPoint.getSignature().toShortString();
		myLogger.info("====>> using @Before: calling method: " + value);
		
		Object[] args = theJoinPoint.getArgs();
		
		for(Object targ: args) {
			myLogger.info("=====>>arguments: " + targ);
		}
	}
	
	@AfterReturning(
			pointcut= "forApply()",
			returning = "theResult"
			)
	public void usingAfterReturning(JoinPoint theJoinPoint, Object theResult) {
		String value = theJoinPoint.getSignature().toShortString();
		myLogger.info("====>> using @Before: calling method: " + value);
		
		myLogger.info("======>>> result: " + theResult);
		
	}
}
