package com.lesson.springdemo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.lesson.springdemo.entity.Teacher;
import com.lesson.springdemo.service.TeacherService;

@Controller
@RequestMapping("/teacher")
public class TeacherController {

	@Autowired
	private TeacherService teacherService;
	@GetMapping("/list")
	public String showTeacherList(Model theModel) {
		List<Teacher> theTeacher = teacherService.getTeachers();
		theModel.addAttribute("teacher", theTeacher);
		return "teacher-list";
	}
	
	@GetMapping("/showTeacherForm")
	public String showForm(Model theModel) {
		theModel.addAttribute("teacherForm", new Teacher());
		return "teacher-form";
	}
	
	@PostMapping("/process")
	public String saveAndBackToList(@ModelAttribute("teacherForm") Teacher theTeacher) {
		teacherService.getTeacher(theTeacher);
		return "redirect:/teacher/list";
	}
	
	@GetMapping("/showFormUpdate")
	public String updateForm(@RequestParam("teacherId") int theId, Model theModel) {
		
		Teacher theTeacher = teacherService.getUpdateTeacher(theId);
		theModel.addAttribute("teacherForm", theTeacher);
		return "teacher-form";
		
	}
	
	@GetMapping("/showDelete")
	public String deleteTeacherHistory(@RequestParam("teacherId") int theId) {
		teacherService.deleteTeacherhistory(theId);
		return "redirect:/teacher/list";
	}
}
