package com.lesson.springdemo.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.lesson.springdemo.entity.Teacher;

@Repository
public class TeacherDAOImpl implements TeacherDAO {

	@Autowired
	private SessionFactory sessionFactory;
	@Override
	public List<Teacher> getTeachers() {
		Session session = sessionFactory.getCurrentSession();
		Query<Teacher> theQuery = session.createQuery("from Teacher");
		List<Teacher> theTeacher = theQuery.getResultList();
		return theTeacher;
	}
	@Override
	public void getTeacher(Teacher theTeacher) {
		Session session = sessionFactory.getCurrentSession();
		session.saveOrUpdate(theTeacher);
	}
	@Override
	public Teacher getUpdateTeacher(int theId) {
		Session session = sessionFactory.getCurrentSession();
		Teacher theTeacher = session.get(Teacher.class, theId);
		return theTeacher;
	}
	@Override
	public void deleteTeacherhistory(int theId) {
		Session session = sessionFactory.getCurrentSession();
		Query theQuery = session.createQuery("delete from Teacher where id=:teacherId");
		theQuery.setParameter("teacherId", theId);
		theQuery.executeUpdate();
		
	}

}
