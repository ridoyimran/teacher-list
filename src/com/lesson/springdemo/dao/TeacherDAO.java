package com.lesson.springdemo.dao;

import java.util.List;

import com.lesson.springdemo.entity.Teacher;

public interface TeacherDAO {

	public List<Teacher> getTeachers();

	public void getTeacher(Teacher theTeacher);

	public Teacher getUpdateTeacher(int theId);

	public void deleteTeacherhistory(int theId);
}
